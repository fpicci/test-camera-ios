//
//  ImgurAPI.swift
//  AR Example iOS
//
//  Created by federico piccirilli on 25/09/18.
//  Copyright © 2018 federico piccirilli. All rights reserved.
//

import Foundation
import Alamofire

class ImgurAPI {
    
    static func upload(image: UIImage){
        
        let imageData = UIImagePNGRepresentation(image)
        let base64Image = imageData?.base64EncodedString(options: .lineLength64Characters)

        let url = "https://api.imgur.com/3/upload"
        
        let parameters = [
            "image": base64Image
        ]
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            if let imageData = UIImageJPEGRepresentation(image, 1) {
                multipartFormData.append(imageData, withName: "image", fileName: "ios.png", mimeType: "image/png")
            }
            for (key, value) in parameters {
                multipartFormData.append((value?.data(using: .utf8))!, withName: key)
            }}, to: url, method: .post, headers: ["Authorization": "Client-ID " + Constants.IMGUR_CLIENT_ID],
                encodingCompletion: { encodingResult in
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.response { response in
                            let json = try? JSONSerialization.jsonObject(with: response.data!, options: .allowFragments) as! [String:Any]
                            print(json ?? "no-data")
                            let imageDic = json?["data"] as? [String:Any]
                            print(imageDic?["link"] ?? "no-data for link")
                        }
                    case .failure(let encodingError):
                        print("error:\(encodingError)")
                    }
        })
        
    }
}
