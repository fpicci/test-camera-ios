//
//  ViewController.swift
//  AR Example iOS
//
//  Created by federico piccirilli on 25/09/18.
//  Copyright © 2018 federico piccirilli. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import FirebaseAnalytics

class ViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet var sceneView: ARSCNView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = true
        
        // Create a new scene
        let scene = SCNScene(named: "art.scnassets/ship.scn")!
        
        // Set the scene to the view
        sceneView.scene = scene
        
        addTakePictureButton()
        
        addAnalytics()
    }
    
    func addTakePictureButton(){
        let button = UIButton()
        button.frame = CGRect(x: self.view.frame.size.width / 2 - 100, y: 66, width: 200, height: 50)
        button.backgroundColor = UIColor.blue
        button.setTitle("Scatta e carica", for: .normal)
        button.addTarget(self, action: #selector(takePicture), for: .touchUpInside)
        self.view.addSubview(button)
    }
    
    func addAnalytics(){
        //
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()

        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    // MARK: - ARSCNViewDelegate
    
/*
    // Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()
     
        return node
    }
*/
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
    
    @objc func takePicture(){
        ImgurAPI.upload(image:sceneView.snapshot())
        }
}

