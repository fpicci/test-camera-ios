# Camera app

App di esempio che usa la libreria arKit per scattare una foto con un aereo in realtà aumentata e caricarla sul un album su imgur. Utilizza firebase analytics.

## Getting Started

### Prerequisites

Xcode 9+, un iPhone che supporta ARKit

## Built With

* [Xcode] - Ambiente di sviluppo
* [Swift 4.2] - Linguaggio di programmazione
* [Alamofire]- Framework per il network layer
* [ARkit]- Ultima Api per usare la camera
* [imgur rest api] - Api per caricare le immagini
* [firebase analytics] - Framework di monitoraggio

## Authors

* **Federico Piccirilli** - *All the work*

## License

This project is licensed under the MIT License

